package damo.cs.upc.edu.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by josepm on 30/6/16.
 */
public class MagatzemCrims {

    // Primera aproximació a la persistència: un Singleton

    private static MagatzemCrims m;

    private List<Crim> crims;

    private MagatzemCrims() {
        crims = new ArrayList<>();
        generaCrims();
    }

    public static MagatzemCrims obtenirMagatzem() {
        if (m == null) {
            m = new MagatzemCrims();
        }

        return m;
    }

    public int getCount() {
        return crims.size();
    }

    public Crim getCrim(int position) {
        return crims.get(position);
    }


    public Crim getCrim(UUID id) {
        for (Crim c : crims) {
            if (c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }


    private void generaCrims() {
        Crim c;
        for (int i = 1; i < 100; i++) {
            c = new Crim();
            c.setTitol("Crim " + i);
            c.setSolucionat(i % 2 == 0);
            crims.add(c);
        }
    }

}
