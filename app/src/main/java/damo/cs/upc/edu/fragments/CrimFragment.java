package damo.cs.upc.edu.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

/**
 * Created by Josep M on 17/06/2016.
 */
public class CrimFragment extends Fragment {

    private static String ARG="Argument";

    private static final String IDCRIM = "id_crim";


    private Crim crim;



    private EditText entradaTitol;
    private CheckBox checkSolucionat;
    private Button botoData;

    static CrimFragment getInstance(){
        CrimFragment f = new CrimFragment();
        Bundle b = new Bundle();
        b.putSerializable(ARG,"");
        f.setArguments(b);
        return f;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        crim = crimAVisualitzar();

    }

    private Crim crimAVisualitzar() {
        UUID idCrim = (UUID) getActivity().getIntent().getSerializableExtra(IDCRIM);
        return MagatzemCrims.obtenirMagatzem().getCrim(idCrim);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.crim_fragment,container,false);
        programaWidgets(v);

        pobla(v);

        return v;

    }



    private void programaWidgets(View v) {
        entradaTitol = (EditText) v.findViewById(R.id.titol_crim);
        checkSolucionat = (CheckBox) v.findViewById(R.id.crime_solved);
        botoData = (Button) v.findViewById(R.id.crime_date);

        entradaTitol.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                crim.setTitol(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        checkSolucionat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                crim.setSolucionat(isChecked);
            }
        });
    }

    private void pobla(View v) {
        entradaTitol.setText(crim.getTitol());
        botoData.setText(crim.getData().toString());
        checkSolucionat.setChecked(crim.getSolucionat());
    }


}
