package damo.cs.upc.edu.fragments;

import java.util.Date;
import java.util.UUID;

/**
 * Created by josepm on 29/6/16.
 * Fragment de: Bill Phillips. “Android Programming: The Big Nerd Ranch Guide (2nd Edition)“. iBooks
 */
public class Crim {
    private UUID id;
    private String titol;
    private Date data;
    private Boolean solucionat;


    public Crim(){
        id = UUID.randomUUID();
        data = new Date();
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getSolucionat() {
        return solucionat;
    }

    public void setSolucionat(Boolean solucionat) {
        this.solucionat = solucionat;
    }


    public UUID getId() {
        return id;
    }


    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }
}
